const {
  company
} = require('../models') // Import user model
const passport = require('passport'); // Import passport
const jwt = require('jsonwebtoken'); // Import jsonwebtoken


// UsersController class declaration
class CompanyController {

  async getAll(req, res) {
    try {
      let getAllCompany = await company.find({})

      return res.status(200).json({
        status: "Success!",
        data: getAllCompany
      })
    } catch (e) {
      console.log(e);
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  async getOne(req, res) {
    try {

      let getCompany = await company.findOne({
        _id: req.params.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getCompany
      })
    } catch (e) {
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  async create(req, res) {
    try {
      let addData = await company.create({
        company_name: req.body.company_name,
        company_size: req.company_size,
        industry: req.body.industry,
        location: {
          address: req.body.address,
          city: req.body.city,
          region: req.body.region
        },
        website: req.body.website,
        detail: req.body.detail,
        culture: req.body.culture,
        galery: req.files.map(file => file.filename)
      })

      let newData = await company.findOne({
        _id: addData.id
      })

      return res.status(200).json({
        status: 'Success',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        message: e
      })
    }
  }

  async update(req, res) {
    try {
      // const {
      //   edit
      // } = req.query

      let newData = {}

      if (req.body.company_name) newData.company_name = req.body.company_name
      if (req.body.company_size) newData.company_size = req.body.company_size
      if (req.body.industry) newData.industry = req.body.industry
      if (req.body.address) newData.address = req.body.address
      if (req.body.city) newData.city = req.body.city
      if (req.body.region) newData.region = req.body.region
      if (req.body.website) newData.website = req.body.website
      if (req.body.detail) newData.detail = req.body.detail
      if (req.body.culture) newData.culture = req.body.culture
      if (req.files) newData.galery = req.files.map(file => file.filename)

      let updateData = await company.findOneAndUpdate({
        _id: req.params.id
      }, {
        $set: newData
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Updated!',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error!',
        message: e
      })
    }
  }

  async delete(req, res) {
    try {
      // const { del } = req.query

      let deleteData = await company.delete({
        _id: req.params.id
      })

      return res.status(200).json({
        status: 'Deleted!',
        data: null
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error!',
        message: e
      })
    }
  }

}

module.exports = new CompanyController; // Export CompanyController
