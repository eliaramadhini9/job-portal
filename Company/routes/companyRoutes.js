const express = require('express'); // Import express
const passport = require('passport'); // Import passport

const router = express.Router(); // Make router
const auth = require('../middlewares/auth'); // Import auth
const CompanyController = require('../controllers/companyController');
const CompanyValidator = require('../middlewares/validators/companyValidator');


// if user go to localhost:3000/signup
router.get('/', CompanyController.getAll)
router.get('/:id', CompanyController.getOne)
router.post('/', CompanyValidator.create, CompanyController.create)
router.put('/:id', CompanyController.update)
router.delete('/:id',  CompanyController.delete)

module.exports = router; // Export router
