const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');
const {
  companyDir
} = require('../middlewares/validators/companyValidator'); // Import UserController

const CompanySchema = new mongoose.Schema({
  company_name: {
    type: String
  },
  company_size: {
    type: String
  },
  industry: {
    type: String
  },
  location: {
    address: {
      type: String,
      required: false
    },
    city: {
      type: String,
      required: true
    },
    region: {
      type: String,
      required: true
    }
  },
  website: {
    type: String
  },
  detail: {
    type: String,
    default: null
  },
  culture: {
    type: String,
    required: false,
    default: null
  },
  galery: {
    type: Array
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

CompanySchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

CompanySchema.set('toJSON', {
  getters: true
})

CompanySchema.path('galery').get(img => {
  console.log(companyDir)
  let imageMap = img.map(value =>
    value = '/' + companyDir[0] + '/' + value)
  return imageMap


  // for (let i=0; i < img.length; i++) {
  //   var check = img[i].match(/images/)
  //
  //   if (check === null) {
  //     return imageMap
  //   } else {
  //     return img
  //   }
  // }
})

module.exports = company = mongoose.model('company', CompanySchema, 'company')
