const {
  company
} = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

const multer = require('multer'); //multipar form-data
const path = require('path'); // to detect path of directory
const crypto = require('crypto'); // to encrypt something
const fs = require('fs'); // to encrypt something

function noSpace(str) {
  var sentence = str.toLowerCase().split(" ")
  return sentence.join("-")
}

let companyDir = []

const uploadDir = '/companies/'; // make images upload to /img/
const storage = multer.diskStorage({
  destination: async function(req, file, cb) {
    let companyName = noSpace(req.body.company_name)

    companyDir.length = 0
    companyDir.push(companyName)

    let dir = './galery' + `/${companyName}/`
    fs.exists(dir, exist => {
      if (!exist) {
        return fs.mkdir(dir, error => cb(error, dir))
      }
      return cb(null, dir)
    })
  },
  filename: function(req, file, cb) {
    crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
    })
  }
})

const upload = multer({
  storage: storage
});

module.exports = {
  create: [
    upload.array('galery'),
    check('galery').custom((value, {
      req
    }) => {
      if (req.files.length <= 0) {
        return false
      } else {
        return true
      }
    }).withMessage('you must select at least 1 image.'),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  update: [
    upload.array('galery'),
    check('galery').custom((value, {
      req
    }) => {
      if (req.files.length <= 0) {
        return false
      } else {
        return true
      }
    }).withMessage('you must select at least 1 image.'),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  companyDir
}