let mongoose = require("mongoose"); // import mongoose
let {
  field
} = require('../models'); // import movie models

//Require the dev-dependencies
let chai = require('chai'); // import chai for testing assert
let chaiHttp = require('chai-http'); // make virtual server to get/post/put/delete
let server = require('../index'); // import app from index
let should = chai.should(); // import assert should from chai
let authentication_key; // movie_id declaration

chai.use(chaiHttp); // use chaiHttp

describe('Field', () => {
  let field_id = '600513ca38232609d343d6c0'
  let user_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoidXNlckB1c2VyLmNvbSIsIl9pZCI6IjYwMWIzNjU4MWExYmZhNjBlODQwNjllYSIsImZ1bGxuYW1lIjoiaW5pIHVzZXIifSwiaWF0IjoxNjEyMzk3MDIwfQ.638KfbXUC2sz5iW7cM44oixNwudVwsUS7BNOonWf9FU'
  let manager_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoibWFuYWdlckBtYW5hZ2VyLmNvbSIsIl9pZCI6IjYwMWIzNWZjOGFlMTI3NjBhYzNhY2FkOCIsImZ1bGxuYW1lIjoibWFuYWdlciJ9LCJpYXQiOjE2MTIzOTc0MjJ9.eAfqrO9zOTOW8z6oyo5O_0MWa5854qonzOmG35GctXI'

  /*
   * Test the /GET route
   */
  describe('/GET All Fields', () => {
    it('it should get all the fields', (done) => {
      chai.request(server) // request to server (index.js)
        .get('/field')
        .end((err, res) => {
          res.should.have.status(200); // Response should have status 200
          res.body.should.be.an('object'); // Body Response should be an object
          res.body.should.have.property('status').eql('Success'); // Body Response should have 'status' property
          res.body.should.have.property('data'); // Body Response should have 'data' property
          res.body.data.should.be.an('array'); // Body Response .data should be an array
          done();
        });
    });
  });

  /*
   * Test the /GET/:id route
   */
  describe('/GET a Field', () => {
    it('it should get a field', (done) => {
      chai.request(server)
        .get('/field/' + field_id)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('object');
          res.body.should.have.property('status').eql('Success');
          res.body.should.have.property('data');
          res.body.data.should.be.an('object');
          res.body.data.should.have.property('image')
          res.body.data.should.have.property('deleted')
          res.body.data.should.have.property('_id')
          res.body.data.should.have.property('fieldName')
          res.body.data.should.have.property('city')
          res.body.data.should.have.property('location')
          res.body.data.should.have.property('price')
          res.body.data.should.have.property('description')
          res.body.data.should.have.property('created_at')
          res.body.data.should.have.property('updated_at')
          res.body.data.should.have.property('id')
          done();
        })
    });
    // it('it should be an error, because field\'s id doesnt exist', (done) => {
    //   chai.request(server)
    //     .get('/field/5ff14910b5997e26688ad851')
    //     .end((err, res) => {
    //       res.should.have.status(422);
    //       res.body.should.be.a('object');
    //       res.body.should.have.property('errors');
    //       done();
    //     })
    // });
  });

  /*
   * Test the /POST route
   */
  describe('/POST field', () => {
    it('it should post new field', (done) => {
      chai.request(server)
        .post('/field/create')
        .set({
          Authorization: `Bearer ${manager_token}`
        })
        // .set('Content-Type', 'multipart/form-data')
        .type('form')
        .field('fieldName', 'this is field name1')
        .field('city', 'this is city')
        .field('location', 'this is location')
        .field('price', 300)
        .field('description', 'this is description')
        .attach('image', './public/images/baskhara-futsal-arena-d2918893cbb09d3cbf2799c951c01ab6.jpg', 'baskhara-futsal-arena-d2918893cbb09d3cbf2799c951c01ab6.jpg')
        .end((err, res) => {
          res.should.have.status(200); // Response should have status 200
          res.body.should.be.an('object'); // Body Response should be an object
          res.body.should.have.property('status').eql('Success'); // Body Response should have 'status' property
          res.body.should.have.property('data'); // Body Response should have 'data' property
          res.body.data.should.be.an('object'); // Body Response .data should be an array
          res.body.data.should.have.property('_id')
          res.body.data.should.have.property('deleted')
          res.body.data.should.have.property('fieldName')
          res.body.data.should.have.property('city')
          res.body.data.should.have.property('location')
          res.body.data.should.have.property('price')
          res.body.data.should.have.property('description')
          res.body.data.should.have.property('created_at')
          res.body.data.should.have.property('updated_at')
          res.body.data.should.have.property('id')
          cfield_id = res.body.data._id;
          done()
        })
    })
  })

  /*
   * Test the /PUT/:id route
   */
  describe('/PUT Field', () => {
    it('it should update a field by id, (ex: edit field name)', (done) => {
      let fieldPut = {
        fieldName: 'this is field name (update)'
      }
      chai.request(server)
        .put('/field/update/' + cfield_id)
        .set({
          Authorization: `Bearer ${manager_token}`
        })
        .send(fieldPut)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('object');
          res.body.should.have.property('status').eql('Success');
          res.body.should.have.property('data');
          res.body.data.should.be.an('object');
          res.body.data.should.have.property('deleted')
          res.body.data.should.have.property('_id')
          res.body.data.should.have.property('fieldName')
          res.body.data.should.have.property('city')
          res.body.data.should.have.property('location')
          res.body.data.should.have.property('price')
          res.body.data.should.have.property('description')
          res.body.data.should.have.property('created_at')
          res.body.data.should.have.property('updated_at')
          res.body.data.should.have.property('id')
          done();
        });
    });
  });

  /*
   * Test the /DELETE/:id route
  //  */
  describe('/DELETE Field', () => {
    it('it should delete a field by id', (done) => {
      chai.request(server)
        .delete('/field/delete/' + cfield_id)
        .set({
          Authorization: `Bearer ${manager_token}`
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('status').eql('Success');
          res.body.should.have.property('data').eql(null);
          done();
        });
    });
  });

});
