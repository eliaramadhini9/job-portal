const express = require('express'); // Import express
const passport = require('passport'); // Import passport

const router = express.Router(); // Make router
const auth = require('../middlewares/auth'); // Import auth
const UserController = require('../controllers/userController'); // Import UserController
const UserValidator = require('../middlewares/validators/userValidator'); // Import UserValidator


// if user go to localhost:3000/signup
router.post('/signup', [UserValidator.signup, function(req, res, next) {
  // will be go to signup in auth
  passport.authenticate('signup', {
    session: false
  }, function(err, user, info) {
    // If error not null
    if (err) {
      return next(err);
    }

    // If user is not exist
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }

    // If not error, it will go to login function in UserController
    UserController.signup(user, req, res);
  })(req, res, next);
}]);

router.post('/employer/signup', [UserValidator.signup, function(req, res, next) {
  // will be go to signup in auth
  passport.authenticate('signupEmployer', {
    session: false
  }, function(err, user, info) {
    // If error not null
    if (err) {
      return next(err);
    }

    // If user is not exist
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }

    // If not error, it will go to login function in EmployerController
    UserController.signup(user, req, res);
  })(req, res, next);
}]);

// if user go to localhost:3000/login
router.post('/login', [UserValidator.login, function(req, res, next) {
  // will be go to login in auth
  passport.authenticate('login', {
    session: false
  }, async function(err, user, info) {
    // If error not null
    if (err) {
      return next(err);
    }

    // If user is not exist
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }

    // If not error, it will go to login function in UserController
    UserController.login(user, req, res);
  })(req, res, next);
}]);

// Request authorization
router.get('/authorization', function(req, res, next) {
  // will be go to login in auth
  passport.authenticate('jwt', {
    session: false
  }, async function(err, user, info) {
    // If error not null
    if (err) {
      return next(err);
    }

    // If user is not exist
    if (!user) {
      res.status(401).json({
        status: 'Error!',
        message: info.message
      });
      return;
    }

    // If not error, it will go to login function in UserController
    UserController.authorization(user, req, res);
  })(req, res, next);
})

router.get('/user', UserController.getAll)
router.get('/user/:id', UserController.getOne)
router.put('/user', UserController.update)

module.exports = router; // Export router
