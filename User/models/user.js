const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const UserSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: false
  },
  last_name: {
    type: String,
    required: false
  },
  email: {
    type: String,
    required: true
  },
  email_verif: {
    type: Boolean,
    default: false
  },
  email_notif: {
    type: Boolean,
    default: false
  },
  password: {
    type: String
  },
  phone: {
    type: String,
    required: true
  },
  picture: {
    type: String,
    required: false
  },
  birth: {
    type: String,
    required: false,
    default: null
  },
  gender: {
    type: String,
    required: false,
    default: null
  },
  status: {
    type: String
  },
  about: {
    type: String,
    default: null
  },
  role: {
    type: String
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

UserSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

UserSchema.path('picture').get((picture) => {
  return '/user/' + picture
})

UserSchema.set('toJSON', {
  getters: true
})

module.exports = user = mongoose.model('user', UserSchema, 'user')
