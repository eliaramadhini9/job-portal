const {
  user,
  education
} = require('../models') // Import user model
const passport = require('passport'); // Import passport
const jwt = require('jsonwebtoken'); // Import jsonwebtoken

// UsersController class declaration
class UserController {

  async getAll(req, res) {
    try {
      let getAllUser = await user.find({})

      return res.status(200).json({
        status: "Success!",
        data: getAllUser
      })
    } catch (e) {
      // If error, it will return the message of error
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  async getOne(req, res) {
    try {
      let getUser = await user.findOne({
        _id: req.params.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getUser
      })
    } catch (e) {
      // If error, it will return the message of error
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  async update(req, res) {
    try {
      const {
        edit
      } = req.query

      let newData = {}

      if (req.body.first_name) newData.first_name = req.body.first_name
      if (req.body.last_name) newData.last_name = req.body.last_name
      if (req.body.phone) newData.phone = req.body.phone
      if (req.body.birth) newData.birth = req.body.birth
      if (req.body.gender) newData.gender = req.body.gender
      if (req.body.status) newData.status = req.body.status
      if (req.body.about) newData.about = req.body.about
      if (req.file) newData.picture = req.file.filename

      let updatedData = await user.findOneAndUpdate({
        _id: edit
      }, {
        $set: newData
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Updated!',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        error: e
      })
    }
  }

  async signup(user, req, res) {
    try {
      // const mailOptions = {
      //   from: 'sokaglints@gmail.com',
      //   to: user.email,
      //   subject: 'Verify Email Address for SOKA',
      //   text: `Hello, ${user.fullname}\n\n` + 'Thanks for registering for an account on SOKA! Before we get started, we just need to confirm that is you. Click the link below to verify your email address:\n\n https://' + req.headers.host + '/auth/confirm_acount?confirmation_token=' + token + '\n\n Best Regards,\n Soka Team'
      // }
      // req.headers.host
      // transporter.sendMail(mailOptions, (err, info) => {
      //   if (err) throw err
      //   console.log('Email sent: ' + info.response);
      // })
      res.status(200).json({
        message: "Register success"
      })
    } catch (e) {
      // If error, it will return the message of error
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  // If user pass the signup or login authorization, it will go to this function to create and get token
  async login(user, req, res) {
    try {
      // Create a varible that will be saved in token
      const body = {
        id: user.id,
        email: user.email
      };

      // Create a token for the user
      const token = jwt.sign({
        user: body
      }, 'secret_password');

      // If success, it will return the message and the token
      return res.status(200).json({
        message: 'Login success!',
        token: token
      });
    } catch (e) {
      // If error, it will return the message of error
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  // This function is to check, Is the user has Authorized or Unauthorized
  async authorization(user, req, res) {
    try {
      // If success, it will be return the user information (id, email, and role)
      return res.status(200).json({
        status: "Success!",
        message: "Authorized!",
        user: user
      })
    } catch (e) {
      // If error, it will return the message of error
      return res.status(401).json({
        status: "Error!",
        message: "Unauthorized!",
      })
    }
  }
}

module.exports = new UserController; // Export UserController
