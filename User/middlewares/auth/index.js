const passport = require('passport'); // Import passport
const localStrategy = require('passport-local').Strategy; // Import localStrategy from passport
const {
  user
} = require('../../models'); // Import user model
const bcrypt = require('bcrypt'); // Import bcrypt
const JWTstrategy = require('passport-jwt').Strategy; // Import JWTstrategy from passport
const ExtractJWT = require('passport-jwt').ExtractJwt; // Import ExtractJWT from passport

// It will be used for signup and create the user
passport.use(
  'signupEmployer',
  new localStrategy({
      'usernameField': 'email',
      'passwordField': 'password', // field for password from req.body.password
      passReqToCallback: true // read other requests
    },
    async (req, email, password, done) => {
      // Create user data
      user.create({
        email: email, // email get from usernameField (req.body.email)
        password: bcrypt.hashSync(password, 10), // password get from passwordField (req.body.passport)
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        phone: req.body.phone,
        role: 'employer'
        // role get from req.body.role
      }).then(result => {
        // If success, it will return authorization with req.user
        return done(null, result, {
          message: 'User created!'
        })
      }).catch(err => {
        // If error, it will return not authorization
        return done(null, false, {
          message: "User can't be created"
        })
      })
    },
  )
);

passport.use(
  'signup',
  new localStrategy({
      'usernameField': 'email',
      'passwordField': 'password', // field for password from req.body.password
      passReqToCallback: true // read other requests
    },
    async (req, email, password, done) => {
      // Create user data
      user.create({
        email: email, // email get from usernameField (req.body.email)
        password: bcrypt.hashSync(password, 10), // password get from passwordField (req.body.passport)
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        phone: req.body.phone,
        role: 'applicant'
        // role get from req.body.role
      }).then(result => {
        // If success, it will return authorization with req.user
        return done(null, result, {
          message: 'User created!'
        })
      }).catch(err => {
        console.log(err);
        // If error, it will return not authorization
        return done(null, false, {
          message: "User can't be created"
        })
      })
    },
  )
);

// It will be used for login
passport.use(
  'login',
  new localStrategy({
      usernameField: 'email', // It will get from req.body.email
      passwordField: 'password' // It will get from req.body.password
    },
    async (email, password, done) => {
      try {
        // Find the user that have been inputed on req.body.email
        const userLogin = await user.findOne({
          email: email
        });

        // If user is not found, it will make Unauthorized and make a message
        if (!userLogin) {
          return done(null, false, {
            message: 'User not found!'
          })
        };

        // If user is found, it will validate the password among the user's input and database
        const validate = await bcrypt.compare(password, userLogin.password);

        // If password wrong, it will make Unauthorized and make a message
        if (!validate) {
          return done(null, false, {
            message: 'Wrong password!'
          })
        };

        if (userLogin.role === 'employer' && userLogin.email_verif === false) {
          return done(null, false, {
            message: 'Email belum di verifikasi. cek emailmu!'
          })
        };

        return done(null, userLogin, {
          message: 'Login success!'
        });
      } catch (e) {
        // If error, it will create this message
        return done(null, false, {
          message: "Can't login!"
        })
      }
    }
  )
);

passport.use(
  'jwt',
  new JWTstrategy({
      secretOrKey: 'secret_password', // It must be same with secret key when created token
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // It will extract token from req.header('Authorization')
    },
    async (token, done) => {
      try {
        // Find the user depends on token that have been extracted
        const userLogin = await user.findOne({
            email: token.user.email
        }, 'id email role');

        // If user is not found, it will make Unauthorized and make a message
        if (!userLogin) {
          return done(null, false, {
            message: 'User not found!'
          })
        };

        // If success, it will return userLogin variable that can be used in the next step
        return done(null, userLogin, {
          message: "Authorized!"
        });
      } catch (e) {
        // If error, it will create this message
        return done(null, false, {
          message: "Unauthorized!"
        });
      }
    }
  )
);

passport.use(
  'manager',
  new JWTstrategy({
      secretOrKey: 'secret_password', // key for jwt
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
    },
    async (token, done) => {
      // find user depend on token.user.email
      const userLogin = await user.findOne({
        email: token.user.email
      })

      // if user.role includes transaksi it will next
      if (userLogin.role.includes('manager')) {
        return done(null, token.user)
      }
      // if user.role not includes transaksi it will not authorization
      return done(null, false)
    }
  )
)
