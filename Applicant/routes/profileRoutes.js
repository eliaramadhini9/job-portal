const express = require('express'); // Import express
const passport = require('passport'); // Import passport

const router = express.Router(); // Make router
const auth = require('../middlewares/auth'); // Import auth
const ProfileController = require('../controllers/profileController'); // Import UserController
const ProfileValidator = require('../middlewares/validators/profileValidator'); // Import UserController


router.get('/basic-info', [passport.authenticate('applicant', { session: false })], ProfileController.get_basic_info)
router.put('/basic-info', [passport.authenticate('applicant', { session: false })], ProfileController.update_basic_info)

router.get('/education', [passport.authenticate('applicant', { session: false })], ProfileController.get_education)
router.post('/education', [passport.authenticate('applicant', { session: false })], ProfileController.add_education)
router.put('/education', [passport.authenticate('applicant', { session: false })], ProfileController.update_education)
router.delete('/education', [passport.authenticate('applicant', { session: false })], ProfileController.delete_education)

router.get('/organizational', [passport.authenticate('applicant', { session: false })], ProfileController.get_organizational)
router.post('/organizational', [passport.authenticate('applicant', { session: false })], ProfileController.add_organizational)
router.put('/organizational', [passport.authenticate('applicant', { session: false })], ProfileController.update_organizational)
router.delete('/organizational', [passport.authenticate('applicant', { session: false })], ProfileController.delete_organizational)

router.get('/work-experience', [passport.authenticate('applicant', { session: false })], ProfileController.get_work_exp)
router.post('/work-experience', [passport.authenticate('applicant', { session: false })], ProfileController.add_work_exp)
router.put('/work-experience', [passport.authenticate('applicant', { session: false })], ProfileController.update_work_exp)
router.delete('/work-experience', [passport.authenticate('applicant', { session: false })], ProfileController.delete_work_exp)

router.get('/resume', [passport.authenticate('applicant', { session: false })], ProfileController.get_resume)
router.post('/resume', [passport.authenticate('applicant', { session: false }), ProfileValidator.add_resume], ProfileController.add_resume)
router.put('/resume', [passport.authenticate('applicant', { session: false }), ProfileValidator.add_resume], ProfileController.update_resume)
router.delete('/resume', [passport.authenticate('applicant', { session: false })], ProfileController.delete_resume)

router.get('/attachment', [passport.authenticate('applicant', { session: false })], ProfileController.get_attach)
router.post('/attachment', [passport.authenticate('applicant', { session: false }), ProfileValidator.add_attach], ProfileController.add_attach)
router.put('/attachment', [passport.authenticate('applicant', { session: false }), ProfileValidator.add_attach], ProfileController.update_attach)
router.delete('/attachment', [passport.authenticate('applicant', { session: false })], ProfileController.delete_attach)

router.get('/award', [passport.authenticate('applicant', { session: false })], ProfileController.get_award)
router.post('/award', [passport.authenticate('applicant', { session: false })], ProfileController.add_award)
router.put('/award', [passport.authenticate('applicant', { session: false })], ProfileController.update_award)
router.delete('/award', [passport.authenticate('applicant', { session: false })], ProfileController.delete_award)

router.get('/preference', [passport.authenticate('applicant', { session: false })], ProfileController.get_pref)
router.post('/preference', [passport.authenticate('applicant', { session: false })], ProfileController.add_pref)
router.put('/preference', [passport.authenticate('applicant', { session: false })], ProfileController.update_pref)
router.delete('/preference', [passport.authenticate('applicant', { session: false })], ProfileController.delete_pref)

router.get('/skill', [passport.authenticate('applicant', { session: false })], ProfileController.get_skill)
router.post('/skill', [passport.authenticate('applicant', { session: false })], ProfileController.add_skill)
router.put('/skill', [passport.authenticate('applicant', { session: false })], ProfileController.update_skill)
router.delete('/skill', [passport.authenticate('applicant', { session: false })], ProfileController.delete_skill)

module.exports = router; // Export router
