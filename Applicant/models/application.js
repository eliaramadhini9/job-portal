const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const ApplicationSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  vacancy: {
    type: mongoose.Schema.Types.Object
  },
  resume:{
    type: String
  },
  cover_letter:{
    type: String,
    required: false
  },
  status: {
    type: String,
    default: 'applied'
  }
}, {
  timestamps: {
    submmittedAt: 'createdAt'
  },
  versionKey: false
})

ApplicationSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

ApplicationSchema.path('resume').get((resume) => {
  return '/applied/' + resume
})

ApplicationSchema.set('toJSON', {
  getters: true
})

module.exports = application = mongoose.model('application', ApplicationSchema, 'application')
