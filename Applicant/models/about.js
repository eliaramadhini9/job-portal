const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const AboutSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  about: {
    type: String,
    required: false
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

AboutSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

AboutSchema.set('toJSON', {
  getters: true
})

module.exports = about = mongoose.model('about', AboutSchema, 'about')
