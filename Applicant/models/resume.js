const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');
const { userID } = require('../middlewares/validators/profileValidator'); // Import UserController

const ResumeSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  resume: {
    type: String,
    required: false
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

ResumeSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

ResumeSchema.set('toJSON', {
  getters: true
})

ResumeSchema.path('resume').get((resume) => {
    return '/user/' + userID[0] + '/' + resume
})

module.exports = resume = mongoose.model('resume', ResumeSchema, 'resume')
