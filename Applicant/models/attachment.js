const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');
const { userID } = require('../middlewares/validators/profileValidator'); // Import UserController

const AttachmentSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  portfolio: {
    type: String,
    required: false
  },
  linkedin: {
    type: String,
    required: false
  },
  git: {
    type: String,
    required: false
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

AttachmentSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

AttachmentSchema.set('toJSON', {
  getters: true
})

AttachmentSchema.path('portfolio').get((portfolio) => {
  return '/user/' + userID[0] + '/' + portfolio
})

module.exports = attachment = mongoose.model('attachment', AttachmentSchema, 'attachment')
