const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const OrganizationalSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  organization: {
    type: String
  },
  role: {
    type: String
  },
  start_date: {
    type: Date
  },
  end_date: {
    type: Date
  },
  active:{
    type: Boolean
  },
  add_info: {
    type: String
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

OrganizationalSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

module.exports = organizational = mongoose.model('organizational', OrganizationalSchema, 'organizational')
