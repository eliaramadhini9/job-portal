const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const PreferenceSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  preference: {
    type: Array
  },
  salary_expect: {
    type: mongoose.Schema.Types.Decimal128
  },
  work_loc: {
    type: Array
  },
  remote:{
    type: Boolean
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

PreferenceSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

module.exports = preference = mongoose.model('preference', PreferenceSchema, 'preference')
