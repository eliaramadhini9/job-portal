const mongoose = require('mongoose'); // Import mongoose

const uri = "mongodb://localhost:27017/job_portal"; // suppliers database mongodb

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false }); // Connect to mongodb database

const education = require('./education');
const skill = require('./skill');
const work_experience = require('./work_experience');
const award = require('./award');
const attachment = require('./attachment');
const resume = require('./resume');
const organizational = require('./organizational');
const preference = require('./preference');
const application = require('./application');
// const notification = require('./notification');

module.exports = { education, skill, work_experience, award, attachment, resume, organizational, preference, application }; // Export supplier model
