const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const SkillSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  skill: {
    type: Array
  },
  level: {
    type: Array,
    required: true
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

SkillSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

module.exports = skill = mongoose.model('skill', SkillSchema)
