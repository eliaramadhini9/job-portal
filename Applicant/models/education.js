const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const EducationSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  institute: {
    type: String,
    required: true
  },
  degree: {
    type: String,
    required: true
  },
  major: {
    type: String,
    required: true
  },
  start_date: {
    type: Date
  },
  end_date: {
    type: Date
  },
  active:{
    type: Boolean
  },
  add_info: {
    type: String
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

EducationSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

module.exports = education = mongoose.model('education', EducationSchema, 'education')
