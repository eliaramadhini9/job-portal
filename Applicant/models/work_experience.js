const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const ExperienceSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  job_title: {
    type: String
  },
  position: {
    type: String
  },
  company: {
    type: String
  },
  start_date: {
    type: Date
  },
  end_date: {
    type: Date
  },
  active:{
    type: Boolean
  },
  add_info: {
    type: String
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

ExperienceSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

module.exports = experience = mongoose.model('experience', ExperienceSchema, 'experience')
