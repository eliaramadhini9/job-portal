const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const AwardSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  name: {
    type: String,
    required: false
  },
  year: {
    type: String,
    required: false
  },
  add_info: {
    type: String,
    required: false
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

AwardSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

module.exports = award = mongoose.model('award', AwardSchema, 'award')
