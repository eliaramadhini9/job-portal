const {
  user,
  education,
  organizational,
  work_experience,
  award,
  attachment,
  resume,
  skill,
  preference
} = require('../models')
const axios = require('axios'); // Import axios
const https = require('https'); // Import https


class ProfileController {
  async get_basic_info(req, res) {
    try {
      let getAPI = {
        method: 'get',
        url: `http://localhost:3001/user/${req.user.id}`
      };

      let response = await axios(getAPI);
      let getData = response.data;

      return res.status(200).json({
        status: "Success!",
        data: getData.data
      })
    } catch (e) {
      // If error, it will return the message of error
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async update_basic_info(req, res) {
    try {
      const {
        edit
      } = req.query

      let newData = {}

      if (req.body.first_name) newData.first_name = req.body.first_name
      if (req.body.last_name) newData.last_name = req.body.last_name
      if (req.body.phone) newData.phone = req.body.phone
      if (req.body.birth) newData.birth = req.body.birth
      if (req.body.gender) newData.gender = req.body.gender
      if (req.body.status) newData.status = req.body.status
      if (req.body.about) newData.about = req.body.about
      if (req.file) newData.picture = req.file.filename

      let getAPI = {
        method: 'put',
        url: `http://localhost:3001/user?edit=${edit}`,
        data: newData
      };
      let response = await axios(getAPI);
      let getData = response.data;

      return res.status(200).json({
        status: 'Updated!',
        data: getData.data
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        error: e
      })
    }
  }

  async get_education(req, res) {
    try {
      let getData = await education.find({
        user_id: req.user.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async add_education(req, res) {
    try {
      let addData = await education.create({
        user_id: req.user.id,
        institute: req.body.institute,
        degree: req.body.degree,
        major: req.body.major,
        start_date: new Date(req.body.start_date),
        end_date: new Date(req.body.end),
        active: req.body.active,
        add_info: req.body.add_info
      })

      let newData = await education.findOne({
        _id: addData.id
      })

      return res.status(200).json({
        status: "Success!",
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async update_education(req, res) {
    try {
      const {
        edit
      } = req.query

      let newData = {}

      if (req.body.institute) newData.institute = req.body.institute
      if (req.body.degree) newData.degree = req.body.degree
      if (req.body.major) newData.major = req.body.major
      if (req.body.start_date) newData.start_date = new Date(req.body.start_date)
      if (req.body.end_date) newData.end_date = new Date(req.body.end)
      if (req.body.active) newData.active = req.body.active
      if (req.body.add_info) newData.add_info = req.body.add_info

      let updatedData = await education.findOneAndUpdate({
        _id: edit
      }, {
        $set: newData
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Updated!',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        error: e
      })
    }
  }

  async delete_education(req, res) {
    try {
      const {
        del
      } = req.query

      let deleteData = await education.delete({
        _id: del
      })

      return res.status(200).json({
        status: "Deleted!",
        data: null
      })
    } catch (e) {
      // If error, it will return the message of error
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async get_organizational(req, res) {
    try {
      let getData = await organizational.find({
        user_id: req.user.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async add_organizational(req, res) {
    try {
      let addData = await organizational.create({
        user_id: req.user.id,
        organization: req.body.organization,
        role: req.body.role,
        start_date: new Date(req.body.start_date),
        end_date: new Date(req.body.end),
        active: req.body.active,
        add_info: req.body.add_info
      })

      let newData = await organizational.findOne({
        _id: addData.id
      })

      return res.status(200).json({
        status: "Success!",
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async update_organizational(req, res) {
    try {
      const {
        edit
      } = req.query

      let newData = {}

      if (req.body.organization) newData.organization = req.body.organization
      if (req.body.role) newData.role = req.body.role
      if (req.body.start_date) newData.start_date = new Date(req.body.start_date)
      if (req.body.end_date) newData.end_date = new Date(req.body.end)
      if (req.body.active) newData.active = req.body.active
      if (req.body.add_info) newData.add_info = req.body.add_info

      let updatedData = await organizational.findOneAndUpdate({
        _id: edit
      }, {
        $set: newData
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Updated!',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        error: e
      })
    }
  }

  async delete_organizational(req, res) {
    try {
      const {
        del
      } = req.query

      let deleteData = await organizational.delete({
        _id: del
      })

      return res.status(200).json({
        status: "Deleted!",
        data: null
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async get_work_exp(req, res) {
    try {
      let getData = await work_experience.find({
        user_id: req.user.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async add_work_exp(req, res) {
    try {
      let addData = await work_experience.create({
        user_id: req.user.id,
        job_title: req.body.job_title,
        company: req.body.company,
        start_date: new Date(req.body.start_date),
        end_date: new Date(req.body.end),
        active: req.body.active,
        add_info: req.body.add_info
      })

      let newData = await work_experience.findOne({
        _id: addData.id
      })

      return res.status(200).json({
        status: "Success!",
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async update_work_exp(req, res) {
    try {
      const {
        edit
      } = req.query

      let newData = {}

      if (req.body.job_title) newData.job_title = req.body.job_title
      if (req.body.company) newData.company = req.body.company
      if (req.body.start_date) newData.start_date = new Date(req.body.start_date)
      if (req.body.end_date) newData.end_date = new Date(req.body.end)
      if (req.body.active) newData.active = req.body.active
      if (req.body.add_info) newData.add_info = req.body.add_info

      let updatedData = await work_experience.findOneAndUpdate({
        _id: edit
      }, {
        $set: newData
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Updated!',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        error: e
      })
    }
  }

  async delete_work_exp(req, res) {
    try {
      const {
        del
      } = req.query

      let deleteData = await work_experience.delete({
        _id: del
      })

      return res.status(200).json({
        status: "Deleted!",
        data: null
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async get_award(req, res) {
    try {
      let getData = await award.find({
        user_id: req.user.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async add_award(req, res) {
    try {
      let addData = await award.create({
        user_id: req.user.id,
        name: req.body.name,
        year: req.body.year,
        add_info: req.body.add_info
      })

      let newData = await award.findOne({
        _id: addData.id
      })

      return res.status(200).json({
        status: "Success!",
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async update_award(req, res) {
    try {
      const {
        edit
      } = req.query

      let newData = {}

      if (req.body.name) newData.name = req.body.name
      if (req.body.year) newData.year = req.body.year
      if (req.body.add_info) newData.add_info = req.body.add_info

      let updatedData = await award.findOneAndUpdate({
        _id: edit
      }, {
        $set: newData
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Updated!',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        error: e
      })
    }
  }

  async delete_award(req, res) {
    try {
      const {
        del
      } = req.query

      let deleteData = await award.delete({
        _id: del
      })

      return res.status(200).json({
        status: "Deleted!",
        data: null
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async get_resume(req, res) {
    try {
      let getData = await resume.find({
        user_id: req.user.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async add_resume(req, res) {
    try {
      let addData = await resume.create({
        user_id: req.user.id,
        resume: req.file.filename
      })

      let newData = await resume.findOne({
        _id: addData.id
      })

      return res.status(200).json({
        status: "Success!",
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async update_resume(req, res) {
    try {
      const {
        edit
      } = req.query

      let updatedData = await resume.findOneAndUpdate({
        _id: edit
      }, {
        $set: {
          resume: req.file.filename
        }
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Updated!',
        data: updatedData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        error: e
      })
    }
  }

  async delete_resume(req, res) {
    try {
      const {
        del
      } = req.query

      let deleteData = await resume.delete({
        _id: del
      })

      return res.status(200).json({
        status: "Deleted!",
        data: null
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async get_attach(req, res) {
    try {
      let getData = await attachment.find({
        user_id: req.user.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async add_attach(req, res) {
    try {
      let addData = await attachment.create({
        user_id: req.user.id,
        portfolio: req.file.filename,
        linkedin: req.body.linkedin,
        git: req.body.git
      })

      let newData = await attachment.findOne({
        _id: addData.id
      })

      return res.status(200).json({
        status: "Success!",
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async update_attach(req, res) {
    try {
      const {
        edit
      } = req.query

      let newData = {}

      if (req.file) newData.portfolio = req.file.filename
      if (req.body.linkedin) newData.linkedin = req.body.linkedin
      if (req.body.git) newData.git = req.body.git

      let updatedData = await attachment.findOneAndUpdate({
        _id: edit
      }, {
        $set: newData
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Updated!',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        error: e
      })
    }
  }

  async delete_attach(req, res) {
    try {
      const {
        del
      } = req.query

      let deleteData = await attachment.delete({
        _id: del
      })

      return res.status(200).json({
        status: "Deleted!",
        data: null
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async get_pref(req, res) {
    try {
      let getData = await preference.find({
        user_id: req.user.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async add_pref(req, res) {
    try {
      let addData = await preference.create({
        user_id: req.user.id,
        preference: req.body.preference,
        salary_expect: req.body.salary_expect,
        work_loc: req.body.work_loc,
        remote: req.body.remote
      })

      let newData = await preference.findOne({
        _id: addData.id
      })

      return res.status(200).json({
        status: "Success!",
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async update_pref(req, res) {
    try {
      const {
        edit
      } = req.query

      let newData = {}

      if (req.body.preference) newData.preference = req.body.preference
      if (req.body.salary_expect) newData.salary_expect = req.body.salary_expect
      if (req.body.work_loc) newData.work_loc = req.body.work_loc
      if (req.body.remote) newData.remote = req.body.remote

      let updatedData = await preference.findOneAndUpdate({
        _id: edit
      }, {
        $set: newData
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Updated!',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        error: e
      })
    }
  }

  async delete_pref(req, res) {
    try {
      const {
        del
      } = req.query

      let deleteData = await preference.delete({
        _id: del
      })

      return res.status(200).json({
        status: "Deleted!",
        data: null
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async get_skill(req, res) {
    try {
      let getData = await skill.find({
        user_id: req.user.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async add_skill(req, res) {
    try {
      let addData = await skill.create({
        user_id: req.user.id,
        skill: req.body.skill,
        level: req.body.level
      })

      let newData = await skill.findOne({
        _id: addData.id
      })

      return res.status(200).json({
        status: "Success!",
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

  async update_skill(req, res) {
    try {
      const {
        edit
      } = req.query

      let newData = {}

      if (req.body.skill) newData.skill = req.body.skill
      if (req.body.level) newData.level = req.body.level

      let updatedData = await skill.findOneAndUpdate({
        _id: edit
      }, {
        $set: newData
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Updated!',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error',
        error: e
      })
    }
  }

  async delete_skill(req, res) {
    try {
      const {
        del
      } = req.query

      let deleteData = await skill.delete({
        _id: del
      })

      return res.status(200).json({
        status: "Deleted!",
        data: null
      })
    } catch (e) {
      return res.status(500).json({
        status: "Error!",
        message: e
      })
    }
  }

}

module.exports = new ProfileController
