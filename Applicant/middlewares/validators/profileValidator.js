const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

const multer = require('multer'); //multipar form-data
const path = require('path'); // to detect path of directory
const crypto = require('crypto'); // to encrypt something
const fs = require('fs');

let userID = []; // make images upload to /img/
const storage = multer.diskStorage({
  destination: async function(req, file, cb) {
    userID.push(req.user.id)
    let dir = {}
    if (user !== undefined) dir = `./public/user/${req.user.id}/`
    fs.exists(dir, exist => {
      if (!exist) {
        return fs.mkdir(dir, error => cb(error, dir))
      }
      return cb(null, dir)
    })
  },
  filename: function(req, file, cb) {
    crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) return cb(err)
      if (file.fieldname === 'resume') {
        cb(null, 'resume-' + raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
      } else if (file.fieldname === 'portfolio') {
        cb(null, 'portfolio-' + raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
      }

    })
  }
})

const upload = multer({
  storage: storage
});

module.exports = {
  add_attach: [
    upload.single('portfolio'),
    // check('portfolio').custom((value, {
    //   req
    // }) => {
    //   if (req.file.size > 5 * 1024 * 1024) {
    //     return false
    //   } else {
    //     return true
    //   }
    // }).withMessage('file size max 5 MB'),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ],
  add_resume: [
    upload.single('resume'),
    check('resume').custom((value, {
      req
    }) => {
      if (req.file.size > 5 * 1024 * 1024) {
        return false
      } else {
        return true
      }
    }).withMessage('file size max 5 MB'),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ],
  userID
};
