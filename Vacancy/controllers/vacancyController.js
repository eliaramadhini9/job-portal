const {
  vacancy
} = require('../models') // Import user model

// UsersController class declaration
class CompanyController {

  async getAll(req, res) {
    try {
      let getAllCompany = await vacancy.find({})

      return res.status(200).json({
        status: "Success!",
        data: getAllCompany
      })
    } catch (e) {
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  async getByCompany(req, res) {
    try {
      const {
        c
      } = req.query

      let getCompany = await vacancy.find({
        company: c
      })

      return res.status(200).json({
        status: "Success!",
        data: getCompany
      })
    } catch (e) {
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  async getOne(req, res) {
    try {
      let getCompany = await vacancy.findOne({
        _id: req.params.id
      })

      return res.status(200).json({
        status: "Success!",
        data: getCompany
      })
    } catch (e) {
      return res.status(401).json({
        status: "Error!",
        message: e
      })
    }
  }

  async create(req, res) {
    try {
      let get = {
        method: 'get',
        url: `http://localhost:3004/companies/${req.params.id}`
      }
      let response = await axios(get)
      let getData = response.data

      let addData = await vacancy.create({
        posted_by: req.user.id,
        company: getData,
        location: req.body.location,
        position: req.body.position,
        salary: {
          min: req.body.salary_min,
          max: req.body.salary_max
        },
        salary_hidden: req.body.salary_hidden,
        experience: {
          min: req.body.exp_min,
          max: req.body.exp_max
        },
        type: req.body.type,
        job_description: req.body.description,
        mandatory_skill: req.body.mandatory_skill,
        ext_skill: req.body.ext_skill,
        is_active: req.body.is_active
      })

      let newData = await vacancy.findOne({
        _id: addData.id
      })

      return res.status(200).json({
        status: 'Success',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error!',
        message: e
      })
    }
  }

  async update(req, res) {
    try {
      let newData = {}

      if (req.body.location) newData.location = req.body.location
      if (req.body.position) newData.position = req.body.position
      if (req.body.salary_min) newData.salary: {
        min = req.body.salary_min
      }
      if (req.body.salary_max) newData.salary: {
        max = req.body.salary_max
      }
      if (req.body.salary_hidden) newData.salary_hidden = req.body.salary_hidden
      if (req.body.exp_min) newData.experience: {
        min = req.body.exp_min
      }
      if (req.body.exp_max) newData.experience: {
        max = req.body.exp_max
      }
      if (req.body.type) newData.type = type: req.body.type
      if (req.body.job_description) newData.job_description = req.body.description
      if (req.body.mandatory_skill) newData.mandatory_skill = req.body.mandatory_skill
      if (req.body.ext_skill) newData.ext_skill = req.body.ext_skill
      if (req.body.is_active) newData.is_active = req.body.is_active

      let updatedData = await vacancy.findOneAndUpdate({
        _id: req.params.id
      }, {
        $set: newData
      }, {
        new: true
      })

      return res.status(200).json({
        status: 'Success',
        data: newData
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error!',
        message: e
      })
    }
  }

  async delete(req, res) {
    try {
      let deleteData = await vacancy.delete({
        _id: req.params.id
      })

      return res.status(200).json({
        status: 'Deleted!',
        data: null
      })
    } catch (e) {
      return res.status(500).json({
        status: 'Error!',
        message: e
      })
    }
  }
}

module.exports = new CompanyController; // Export CompanyController
