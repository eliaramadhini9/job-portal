const express = require('express'); // Import express
const passport = require('passport'); // Import passport
const router = express.Router(); // Make router
const auth = require('../middlewares/auth'); // Import auth
const VacancyController = require('../controllers/vacancyController');


// if user go to localhost:3000/signup
router.get('/', VacancyController.getAll)
router.get('/:id', VacancyController.getOne)
router.post('/', VacancyController.create)
router.put('/:id', VacancyController.update)
router.delete('/:id', VacancyController.delete)

module.exports = router; // Export router
