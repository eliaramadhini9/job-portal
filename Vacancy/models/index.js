const mongoose = require('mongoose'); // Import mongoose

const uri = "mongodb://localhost:27017/job_portal"; // suppliers database mongodb

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false }); // Connect to mongodb database

const vacancy = require('./vacancy');

module.exports = { vacancy }; // Export supplier model
