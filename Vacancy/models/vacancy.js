const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const VacancySchema = new mongoose.Schema({
  posted_by: {
    type: mongoose.Schema.Types.Mixed
  },
  company_name: {
    type: String
  },
  company_hidden: {
    type: Bolean,
    default: false
  },
  location: {
    type: String
  },
  position: {
    type: String,
    required: true
  },
  salary: {
    min: {
      type: Number
    },
    max: {
      type: Number
    }
  },
  salary_hidden: {
    type: Bolean,
    required: false
  },
  experience: {
    min: {
      type: Number
    },
    max: {
      type: Number
    }
  },
  type: {
    type: String,
    required: true
  },
  job_description: {
    type: String
  },
  mandatory_skill: {
    type: Array,
    required: true
  },
  ext_skill: {
    type: Array
  },
  is_active: {
    type: Bolean,
    required: true
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  versionKey: false
})

VacancySchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
})

VacancySchema.set('toJSON', {
  getters: true
})

module.exports = vacancy = mongoose.model('vacancy', VacancySchema, 'vacancy')
